package org.oo.ht;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

// https://stackoverflow.com/questions/16364547/how-to-parse-xml-to-java-object

// SmartPost class for smartpost data
@XmlRootElement(name = "place")
public class SmartPost {

    private int code;
    private String city;
    private String address;
    private String availability;
    private String postOffice;
    private float lat;
    private float lng;
    private GeoPoint location;

    public int getCode() {
        return code;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getAvailability() {
        return availability;
    }

    public String getPostOffice() {
        return postOffice;
    }

    public float getLat() {
        return lat;
    }

    public float getLng() {
        return lng;
    }

    public GeoPoint getLocation() {
        return location;
    }

    @XmlElement
    public void setCode(int code) {
        this.code = code;
    }

    @XmlElement
    public void setCity(String city) {
        this.city = city;
    }

    @XmlElement
    public void setAddress(String address) {
        this.address = address;
    }

    @XmlElement
    public void setAvailability(String availability) {
        this.availability = availability;
    }

    @XmlElement(name = "postoffice")
    public void setPostOffice(String postOffice) {
        this.postOffice = postOffice;
    }

    @XmlElement
    public void setLat(float lat) {
        this.lat = lat;
    }

    @XmlElement
    public void setLng(float lng) {
        this.lng = lng;
    }


    public void setLocation() {
        this.location = new GeoPoint(lat, lng);
    }

    @Override
    public String toString() {
        return postOffice;
    }
}

// GeoPoint class for coordinates
class GeoPoint {

    private float latitude;
    private float longitude;

    GeoPoint(float latitude, float longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }
}