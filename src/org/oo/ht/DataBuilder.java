package org.oo.ht;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

//https://stackoverflow.com/questions/27462441/ignore-root-element-in-jaxb
//https://stackoverflow.com/questions/8186896/how-do-i-parse-this-xml-in-java-with-jaxb

// Singleton, so only one instance exists
// Smartpost holder/controller class
public class DataBuilder {
    private static DataBuilder instance = null;

    private DataBuilder(){};

    public static DataBuilder getInstance() {
        if (instance == null) {
            instance = new DataBuilder();
            System.out.println("Made new DataBuilder instance");
        }
        return instance;
    }
    // Lists to keep track of smartposts and smartposts added to map
    List<SmartPost> smartPosts = new ArrayList<>();
    List<SmartPost> smartPostsOnMap = new ArrayList<>();

    // Initialization method
    public  void FetchSmartPosts(){
        try {

            // Parse xml to text for SmartPost class
            JAXBContext context = JAXBContext.newInstance(XmlRoot.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            // Local file
            //XmlRoot root = (XmlRoot) unmarshaller.unmarshal(new File("src/org/oo/ht/fi_apt.xml"));

            // From online
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
            XmlRoot root = (XmlRoot) unmarshaller.unmarshal(url);

            smartPosts = root.getSmartPosts();

            // Run setLocation so GeoPoints get populated
            for (SmartPost smartPost : smartPosts) {
                smartPost.setLocation();
            }

            // Test output
            //Marshaller marshaller = context.createMarshaller();
            //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            //marshaller.marshal(root, System.out);

        } catch (JAXBException | MalformedURLException e) {
            e.printStackTrace();
        }

    }
}
