package org.oo.ht;


import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

// http://fxexperience.com/2011/05/maps-in-javafx-2-0/
// https://stackoverflow.com/questions/12153622/how-to-close-a-javafx-application-on-window-close

// Main class with GUI
public class GUI extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{

        // Initialize "Main" class which is used for initializations
        Main mainProgram = new Main();
        mainProgram.mainProgram();

        // Create main window
        Parent root = FXMLLoader.load(getClass().getResource("mainWindow.fxml"));
        primaryStage.setTitle("TIMO");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();

        // Write log to file when main window exits
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                String time = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
                try {
                    BufferedWriter writer= new BufferedWriter(new FileWriter(time + "-log.txt", false));
                    for (String logEntry : Storage.getInstance().log) {
                        writer.write(logEntry);
                        writer.newLine();
                    }
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


    }


    public static void main(String[] args) {
        launch(args);
    }



}
