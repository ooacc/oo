package org.oo.ht;

import java.util.ArrayList;
import java.util.List;

// Singleton
// Package/item/log holder/controller class
public class Storage {
    private static Storage instance = null;

    private Storage(){};

    public static Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
            System.out.println("Made new Storage instance");
        }
        return instance;
    }

    // Holding lists
    List<Item> items = new ArrayList<Item>();
    List<Package> packets = new ArrayList<Package>();
    List<String> log = new ArrayList<>();

    // Create couple of items on startup
    public void InitializeStorage(){
        CreateItem(5, 5, 5, 5, "Pizza", true);
        CreateItem(5, 5, 5, 5, "Oranges", true);
        CreateItem(12, 27, 40, 8, "Case of beer", false);
        CreateItem(100, 120, 110, 800, "Pallet of beer", false);
    }

    public Item CreateItem(int width, int length, int height, float mass, String name, boolean breakable) {
        Item item = new Item(width, length, height, mass, name, breakable);
        items.add(item);
        System.out.println("New item created.");
        AddLogEntry(item, null, 0);

        return item;
    }

    void AddLogEntry(Item item, Package packet, double distance) {
        if (item != null) {
            String logEntry = "New item created, name: " + item.name + ", total items: " + Item.getItemCounter() + "\n";
            log.add(logEntry);
        }
        if (packet != null) {
            if (distance > 0) {
                String logEntry = "New package sent, item name: " + packet.getItem().name + "From: " + packet.getOrigin().getPostOffice() + ", To: " + packet.getDestination().getPostOffice() + ", distance: " + distance + " km, total number of packages sent: " + Package.getPackagesSentCounter() + "\n";
                log.add(logEntry);
            }
            String logEntry = "New package made, item name: " + packet.getItem().name + ", From: " + packet.getOrigin().getPostOffice() + ", To: " + packet.getDestination().getPostOffice() + ", total number of packages made: " + Package.getPackagesMadeCounter() + "\n";
            log.add(logEntry);
        }
    }

    public Package CreatePackage(Item item, int postClass, SmartPost origin, SmartPost destination) {
        if (postClass == 1){
            Package packet = new FirstClass();

                if (checkPackage(packet, item)) {
                    // Set variables in package
                    packet.setItem(item);
                    packet.setOrigin(origin);
                    packet.setDestination(destination);

                    System.out.println("New first class package created");
                    System.out.println("Name: " + item.getName());

                    packets.add(packet);
                    AddLogEntry(null, packet, 0);
                    return packet;
                } else {
                    System.out.println("Package creation failed");

                }
        }
        else if (postClass == 2) {
            Package packet = new SecondClass();

            if (checkPackage(packet, item)) {
                // Set variables in package
                packet.setItem(item);
                packet.setOrigin(origin);
                packet.setDestination(destination);

                System.out.println("New second class package created");
                System.out.println("Name: " + item.getName());

                packets.add(packet);
                AddLogEntry(null, packet, 0);
                return packet;
            } else {
                System.out.println("Package creation failed");
            }
        }
        else {
            Package packet = new ThirdClass();

            if (checkPackage(packet, item)) {
                // Set variables in package
                packet.setItem(item);
                packet.setOrigin(origin);
                packet.setDestination(destination);

                System.out.println("New third class package created");
                System.out.println("Name: " + item.getName());

                packets.add(packet);
                AddLogEntry(null, packet, 0);
                return packet;
            } else {
                System.out.println("Package creation failed");
            }
        }
        return null;
    }

    // Checks that item is in spec for postal class
    private boolean checkPackage(Package packet, Item item){
            if (item.getWidth() < packet.maxWidth) {
                System.out.println("Width ok");
                if (item.getHeight() < packet.maxHeight) {
                    System.out.println("Height ok");
                    if (item.length < packet.maxLength) {
                        System.out.println("Length ok");
                        if (item.mass < packet.maxMass) {
                            System.out.println("Mass ok");
                            if (item.isBreakable() && packet.willBreak) {
                                System.out.println("Package would break when shipped");
                            }
                            else {
                                return true;
                            }
                        }
                        else {
                            System.out.println("Mass not ok");
                        }
                    }
                    else {
                        System.out.println("Length not ok");
                    }
                }
                else {
                    System.out.println("Height not ok");
                }
            }
            else {
                System.out.println("Width not ok");
            }
        return false;
    }
}
