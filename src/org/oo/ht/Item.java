package org.oo.ht;

// Item class with constructor and getters
// Could have used volume instead of W x L x H
public class Item {
    protected int width;
    protected int length;
    protected int height;
    protected float mass;
    protected String name;
    protected boolean breakable;

    private static int itemCounter = 0;

    public Item(int w, int l, int h, float m, String n, boolean b) {
        this.width = w;
        this.length = l;
        this.height = h;
        this.mass = m;
        this.name = n;
        this.breakable = b;

        itemCounter++;
    }

    public static int getItemCounter() {
        return itemCounter;
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    public int getHeight() {
        return height;
    }

    public float getMass() {
        return mass;
    }

    public String getName() {
        return name;
    }

    public boolean isBreakable() {
        return breakable;
    }

    @Override
    public String toString() {
        return name;
    }
}
