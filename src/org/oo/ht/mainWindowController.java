package org.oo.ht;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import javax.xml.crypto.Data;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class mainWindowController {
    private WebEngine engine;

    @FXML
    private Button updatePackagesButton;

    @FXML
    private TextField widthTextField;

    @FXML
    private TextField itemNameTextField;

    @FXML
    private TextField lengthTextField;

    @FXML
    private Button createPackageButton;

    @FXML
    private Button addSmartPostButton;

    @FXML
    private TextField postClassTextField;

    @FXML
    private ComboBox<SmartPost> smartPostMenuButton;

    @FXML
    private TextField massTextField;

    @FXML
    private ComboBox<Package> showPackagesButton;

    @FXML
    private Button removeRoutesButton;

    @FXML
    private WebView webView;

    @FXML
    private Button packageLogButton;

    @FXML
    private TextField heightTextField;

    @FXML
    private CheckBox breakableCheckbox;

    @FXML
    private Button sendPackageButton;
    @FXML
    private Button displayInfoButton;

    @FXML
    void createPackageButtonAction(ActionEvent event) {
        // Creates new window for creating packages
        try {
            Stage createWindow = new Stage();
            Parent create = FXMLLoader.load(getClass().getResource("createPackage.fxml"));
            createWindow.setScene(new Scene(create, 500, 440));
            createWindow.setTitle("Create new package");
            createWindow.show();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @FXML
    void addSmartPostButtonAction(ActionEvent event) {
        SmartPost smartPost = smartPostMenuButton.getSelectionModel().getSelectedItem();
        System.out.println("Selected: " + smartPost);
        if (smartPost != null) {
            DataBuilder.getInstance().smartPostsOnMap.add(smartPost); // Add smartpost to list which keeps track of smartposts on map

            // Adds red marker on map
            engine.executeScript("document.goToLocation('" + smartPost.getAddress() + ", " + smartPost.getCode() + " " + smartPost.getCity() + "', '" + smartPost.getPostOffice() + " " + smartPost.getAvailability() + "', 'red')");
        }
    }

    @FXML
    void packageLogButtonAction(ActionEvent event) {
        // Create new window for log
        try {
            Stage createWindow = new Stage();
            Parent log = FXMLLoader.load(getClass().getResource("packageLog.fxml"));
            createWindow.setScene(new Scene(log, 800, 400));
            createWindow.setTitle("Log");
            createWindow.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void sendPackageButtonAction(ActionEvent event) {
        GeoPoint org;
        GeoPoint dest;
        double dist;
        try {
            org = showPackagesButton.getSelectionModel().getSelectedItem().origin.getLocation();
            dest = showPackagesButton.getSelectionModel().getSelectedItem().destination.getLocation();
        } catch (NullPointerException e) {
            System.out.println("Nothing to send.");
            return;
        }

        // Coordinates for start
        List<Float> coords = new ArrayList<>();
        coords.add(org.getLatitude());
        coords.add(org.getLongitude());

        // Coordinates for end
        coords.add(dest.getLatitude());
        coords.add(dest.getLongitude());

        // Get distance from index.html, so no first class packages get sent if the distance is over 150km
        try {
            dist = (double) engine.executeScript("document.distance(" + coords + ")");

            if (showPackagesButton.getSelectionModel().getSelectedItem().postClass == 1) {
                if (dist > 150) {
                    System.out.println("Distance is too large. Max distance for class 1 is 150km. Package not sent.");
                    return;
                }
            }

        } catch (ClassCastException e) {
            // If distance is 0 error
            System.out.println("Can't send from smartpost to same smartpost.");
            return;
        }

        // Add sent to counter
        Package.addPackagesSentCounter();

        // Add log line
        Storage.getInstance().AddLogEntry(null, showPackagesButton.getSelectionModel().getSelectedItem(), dist);

        // Create "driving" path from start to end
        engine.executeScript("document.createPath(" + coords + ", 'red', " + showPackagesButton.getSelectionModel().getSelectedItem().postClass + ")");
        System.out.println("Sent package");

    }

    @FXML
    void removeRoutesButtonAction(ActionEvent event) {
        // Removes routes from map
        engine.executeScript("document.deletePaths()");
    }

    @FXML
    void displayInfoButtonAction(ActionEvent event) {
        // Display package information in main window
        try {
            Item item = showPackagesButton.getSelectionModel().getSelectedItem().getItem();

            itemNameTextField.setText(item.getName());
            widthTextField.setText(String.valueOf(item.getWidth()));
            lengthTextField.setText(String.valueOf(item.getLength()));
            heightTextField.setText(String.valueOf(item.getHeight()));
            massTextField.setText(String.valueOf(item.getMass()));
            breakableCheckbox.setSelected(item.isBreakable());

            postClassTextField.setText(String.valueOf(showPackagesButton.getSelectionModel().getSelectedItem().getPostClass()));
        } catch (NullPointerException e) {
            System.out.println("Nothing to display.");
        }
    }


    @FXML
    void updatePackagesButtonAction(ActionEvent event) {
        // Populates package combobox
        List<Package> packets = Storage.getInstance().packets;
        showPackagesButton.getItems().clear();
        showPackagesButton.getItems().addAll(packets);
    }

    @FXML
    private void initialize()
    {
        // Add map
        engine = webView.getEngine();
        String htmlPath = Paths.get("").toAbsolutePath().toString();
        System.out.println("Html file path: file:///" + htmlPath + "/index.html");
        engine.load("file:///" + htmlPath + "/index.html");

        // Populate smartpost combobox
        for (SmartPost smartPost : DataBuilder.getInstance().smartPosts) {
            smartPostMenuButton.getItems().add(smartPost);
        }
    }

}
