package org.oo.ht;

// Main class which initializes smartpost controller class, databuilder
// Also initializes storage class for packages/items/log, Storage
public class Main {
    public void mainProgram() {
        DataBuilder dataBuilder = DataBuilder.getInstance();
        dataBuilder.FetchSmartPosts();
        System.out.println("Fetched smartposts");

        Storage storage = Storage.getInstance();
        storage.InitializeStorage();
        System.out.println("Initialized storage");
    }

}
