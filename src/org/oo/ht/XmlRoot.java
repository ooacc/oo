package org.oo.ht;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

// Created to get rid of unexpected element error
// https://stackoverflow.com/questions/27462441/ignore-root-element-in-jaxb

@XmlRootElement(name = "pprv2")
public class XmlRoot {

    //@XmlElementWrapper(name = "pprv2")
    @XmlElement(name = "place")
    private List<SmartPost> smartPost;
    //private List<SmartPost> smartPosts;

    public List<SmartPost> getSmartPosts() {
        return smartPost;
    }

}
