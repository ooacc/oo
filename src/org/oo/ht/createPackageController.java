package org.oo.ht;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextField;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class createPackageController {

    @FXML
    private TextField widthTextField;

    @FXML
    private ComboBox<Item> chooseItemMenuButton;

    @FXML
    private TextField lengthTextField;

    @FXML
    private Button createPackageButton;

    @FXML
    private TextField nameTextField;

    @FXML
    private ComboBox<String> originCityMenuButton;

    @FXML
    private ComboBox<SmartPost> destinationSmartPostMenuButton;

    @FXML
    private TextField massTextField;

    @FXML
    private ComboBox<String> destinationCityMenuButton;

    @FXML
    private Button cancelButton;

    @FXML
    private TextField heightTextField;

    @FXML
    private ComboBox<String> postalClassMenuButton;

    @FXML
    private CheckBox breakableCheckbox;

    @FXML
    private ComboBox<SmartPost> originSmartPostMenuButton;

    @FXML
    private Button createNewItemButton;



    @FXML
    void createNewItemButtonAction(ActionEvent event) {
        try {
            // Create new item
            Item item = Storage.getInstance().CreateItem(Integer.parseInt(widthTextField.getText()), Integer.parseInt(lengthTextField.getText()), Integer.parseInt(heightTextField.getText()), Float.parseFloat(massTextField.getText()), nameTextField.getText(), breakableCheckbox.isSelected());

            // Add to combobox
            chooseItemMenuButton.getItems().add(item);
        } catch (NumberFormatException e) {
            System.out.println("String/null in number field, creation failed");
        }


    }

    @FXML
    void cancelButtonAction(ActionEvent event) {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }



    @FXML
    void createPackageButtonAction(ActionEvent event) {
        try {
            // Create new package
            Package packet = Storage.getInstance().CreatePackage(chooseItemMenuButton.getValue(), Integer.parseInt(postalClassMenuButton.getValue()), originSmartPostMenuButton.getValue(), destinationSmartPostMenuButton.getValue());

            // Close window
            Stage stage = (Stage) cancelButton.getScene().getWindow();
            stage.close();
        } catch (NumberFormatException | NullPointerException e) {
            System.out.println("Item/post class/origin/destination is null, can't create package.");
        }

    }

    @FXML
    private void initialize()
    {
        // Fill item combobox
        for (Item item : Storage.getInstance().items) {
            chooseItemMenuButton.getItems().add(item);
        }

        // Fill smartpost comboboxes
        for (SmartPost smartPost : DataBuilder.getInstance().smartPostsOnMap) {
            destinationSmartPostMenuButton.getItems().add(smartPost);
            originSmartPostMenuButton.getItems().add(smartPost);
        }

        // Fill postage class combobox
        postalClassMenuButton.getItems().addAll("1", "2", "3");


    }

}
