package org.oo.ht;

// Package class, mostly for max sizes
public class Package {
    protected int maxWidth;
    protected int maxLength;
    protected int maxHeight;
    protected float maxMass;
    protected boolean willBreak;
    protected float maxDistance;
    protected int postClass;
    protected Item item;
    protected SmartPost origin;
    protected SmartPost destination;

    private static int packagesMadeCounter = 0;
    private static int packagesSentCounter = 0;


    public boolean getWillBreak() {
        return willBreak;
    }

    public float getMaxDistance() {
        return maxDistance;
    }

    public Item getItem() {
        return item;
    }

    public int getPostClass() {
        return postClass;
    }


    public void setItem(Item item) {
        this.item = item;

        packagesMadeCounter++;
    }

    public void setOrigin(SmartPost origin) {
        this.origin = origin;
    }

    public void setDestination(SmartPost destination) {
        this.destination = destination;
    }

    public SmartPost getOrigin() {
        return origin;
    }

    public SmartPost getDestination() {
        return destination;
    }

    public static int getPackagesMadeCounter() {
        return packagesMadeCounter;
    }

    public static int getPackagesSentCounter() {
        return packagesSentCounter;
    }

    public static void addPackagesSentCounter() {
        packagesSentCounter++;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}


// Different postal classes, with different settings
class FirstClass extends Package {
    public FirstClass() {
        this.postClass = 1;
        this.maxDistance = 150;
        this.willBreak = true;

        this.maxWidth = 100;
        this.maxHeight = 100;
        this.maxLength = 100;
        this.maxMass = 100;
    }
}

class SecondClass extends Package {
    public SecondClass() {
        this.postClass = 2;
        this.maxDistance = 9001;
        this.willBreak = false;

        this.maxWidth = 40;
        this.maxHeight = 40;
        this.maxLength = 40;
        this.maxMass = 10;
    }
}

class ThirdClass extends Package {
    public ThirdClass() {
        this.postClass = 3;
        this.maxDistance = 9001;
        this.willBreak = false;

        this.maxWidth = 1000;
        this.maxHeight = 1000;
        this.maxLength = 1000;
        this.maxMass = 1000;
    }
}