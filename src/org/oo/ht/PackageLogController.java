package org.oo.ht;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;


public class PackageLogController {

    @FXML
    private TextArea packageLogTextArea;

    @FXML
    private void initialize() {
        packageLogTextArea.clear();

        // Gets log from Storage and prints it
        for (String logEntry : Storage.getInstance().log) {
            packageLogTextArea.setText(packageLogTextArea.getText() + logEntry);
        }
    }

}
